#include <pcap.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

struct ethernet_hdr {
  u_int8_t mac_dest[6];
  u_int8_t mac_src[6];
  uint16_t ether_type;
};
  
u_int16_t wrapp_arround_add(u_int16_t a, u_int16_t b) {
  if ((int)a + (int)b > 0xffff) {
    return (a + b) % 0x010000 + 1;
  } else {
    return (a + b) % 0x010000;
  }
};
void usage() {
  printf("syntax: tcp_block <interface> <host>\n");
  printf("sample: tcp_block eth0 test.gilgil.net\n");
}

int is_string_in_arr(char* buf, const char* target_arr[]) {
	int ret = 0;
	for (int i = 0; i < (sizeof(target_arr) / sizeof(target_arr[0])) ; i++ ) {
		if(!strncmp(buf, target_arr[i], strlen(target_arr[i]))) {
			ret = 1;
		}
	}
	return ret;
}

const char* methods[] = {"GET", "PUT", "POST", "DELETE", "OPTIONS", "HEAD"};

// directiton: if 1 -> forward, -1 -> backward
// type: if 1 -> fin, if 0 -> rst
u_char* make_response_packet(ethernet_hdr req_eth_hdr, ip req_ip_hdr, tcphdr req_tcp_hdr, int ack_offset, int direction, int type, char * tcp_payload = NULL) {

  int payload_len = 0;
  if (type == 1 && tcp_payload) {
    payload_len = strlen(tcp_payload);
  }
  u_char* response_packet = (u_char *)malloc(sizeof(ethernet_hdr) + sizeof(ip) + sizeof(tcphdr) + payload_len);
  struct ethernet_hdr eth_hdr;
  struct ip ip_hdr;
  struct tcphdr tcp_hdr;

  ////////////////////// ethernet set ///////////////////////
  memcpy(&eth_hdr, &req_eth_hdr, sizeof(ethernet_hdr));
  if (direction == -1) { // backward
    memcpy(eth_hdr.mac_src, req_eth_hdr.mac_dest, (sizeof(u_int8_t)) * 6);
    memcpy(eth_hdr.mac_dest, req_eth_hdr.mac_src, (sizeof(u_int8_t)) * 6);    
  }
  eth_hdr.ether_type = htons(0x0800);

  ////////////////////// ip set //////////////////////////
  memcpy(&ip_hdr, &req_ip_hdr, sizeof(ip));
  if (direction == -1) { // backward    
    ip_hdr.ip_dst = req_ip_hdr.ip_src;
    ip_hdr.ip_src = req_ip_hdr.ip_dst;
  }
  ip_hdr.ip_v = 4;
  ip_hdr.ip_hl = 5;
  ip_hdr.ip_len = htons(sizeof(ip) + sizeof(tcphdr) + payload_len);
  ip_hdr.ip_p = 6;
  ip_hdr.ip_sum = 0x0000;
  u_int16_t ip_sum = 0;
  for (int i = 0; i < 10 ; i ++) {
    ip_sum = wrapp_arround_add(ip_sum, ntohs(*(u_int16_t*)(&ip_hdr + i*2)));
  }
  ip_hdr.ip_sum = htons(~ip_sum);

  /////////////////// tcp set //////////////////////////////
  memcpy(&tcp_hdr, &req_tcp_hdr, sizeof(tcphdr));
  if (direction == -1) { // backward
    tcp_hdr.th_sport = req_tcp_hdr.th_dport;
    tcp_hdr.th_dport = req_tcp_hdr.th_sport;
  }
  tcp_hdr.th_off = 5;
  tcp_hdr.th_urp = 0x0000;
  if (type == 1) {
    if (tcp_payload) {
      memcpy(response_packet + sizeof(ethernet_hdr) + sizeof(ip) + sizeof(tcphdr), tcp_payload, payload_len);
    }
    tcp_hdr.th_flags = TH_ACK + TH_FIN;
  } else {
    tcp_hdr.th_flags = TH_ACK + TH_RST;
  }
  if (direction == -1) { // backward
    tcp_hdr.th_ack = htonl(ntohl(req_tcp_hdr.th_seq) + ack_offset);
    tcp_hdr.th_seq = htonl(ntohl(req_tcp_hdr.th_ack));
  } else { // forward
    tcp_hdr.th_seq = htonl(ntohl(req_tcp_hdr.th_seq) + ack_offset);
    tcp_hdr.th_ack = htonl(ntohl(req_tcp_hdr.th_ack));
  }
  u_int16_t tcp_sum = 0;
  tcp_sum = wrapp_arround_add(tcp_sum, (u_int16_t)(ntohs(ip_hdr.ip_src.s_addr) >> 8));
  tcp_sum = wrapp_arround_add(tcp_sum, (u_int16_t)(ntohs(ip_hdr.ip_src.s_addr) && 0x0f));
  tcp_sum = wrapp_arround_add(tcp_sum, (u_int16_t)(ntohs(ip_hdr.ip_dst.s_addr) >> 8));
  tcp_sum = wrapp_arround_add(tcp_sum, (u_int16_t)(ntohs(ip_hdr.ip_dst.s_addr) && 0x0f));
  tcp_sum = wrapp_arround_add(tcp_sum, ntohs(0x0006));
  tcp_sum = wrapp_arround_add(tcp_sum, ntohs(sizeof(tcphdr) + payload_len));
  tcp_hdr.th_sum = htons(~tcp_sum);
  // merge
  memcpy(response_packet, &eth_hdr, sizeof(ethernet_hdr));
  memcpy(response_packet + sizeof(ethernet_hdr), &ip_hdr, sizeof(ip));
  memcpy(response_packet + sizeof(ethernet_hdr) + sizeof(ip), &tcp_hdr, sizeof(tcphdr));

  return response_packet;
};

int main(int argc, char* argv[]) {

  typedef struct _arp_hdr arp_hdr;

  // for pcap_receive
  struct pcap_pkthdr* header;
  const u_char* packet;

  if (argc != 3) {
    usage();
    return -1;
  }

  // request
  struct ip ip_header;
  struct tcphdr tcp_header;
  struct ethernet_hdr ethernet_header;
  ip_header.ip_v = htons(0x0001); // request

  // pcap open
  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }
  
  int pointer;
  printf("Got interface: %s\n", argv[1]);
  
  while (true) { // find reply

    int res = pcap_next_ex(handle, &header, &packet);
    if (res == -1 || res == -2) { 
      printf("err\n");
      return -1;
    }
    if (res == 0) {
      printf("not received\n");
      continue;
    }
    memcpy(&ethernet_header, packet, sizeof(ethernet_hdr));
    pointer = sizeof(ethernet_hdr);
    if (ntohs(ethernet_header.ether_type) != 0x0800) { // if not ipv4 continue
      continue;
    }

    memcpy(&ip_header, &packet[pointer], sizeof(ip));
    pointer += ip_header.ip_hl * 4;
    if (ip_header.ip_p != 6) { // if not tcp continue
      continue;
    }

    memcpy(&tcp_header, &packet[pointer], sizeof(tcp_header));
    pointer += tcp_header.th_off * 4;
  	if (!is_string_in_arr((char *)packet + pointer, methods)) { // if not http request, continue
      continue;
    }
    int ack_offset = header->caplen - pointer;
    char* hostname = strstr((char*)(packet + pointer), "Host: ") + 6;
    if (memcmp(argv[2], hostname, strlen(argv[2]))) {  // if not blockhost, continue
      continue;
    }

    // block logic
    char *content = "HTTP/1.1 301 Moved Permanently\r\nContent-length: 0\r\nLocation: https://warning.or.kr/\r\n\00";
    printf("\nblock\n");
    u_char *response_packet = make_response_packet(ethernet_header, ip_header, tcp_header, ack_offset, 1, 0);
    pcap_sendpacket(handle, response_packet, sizeof(ethernet_hdr)+sizeof(ip)+sizeof(tcphdr));
    free(response_packet);
    response_packet = make_response_packet(ethernet_header, ip_header, tcp_header, ack_offset, -1, 1, content);
    pcap_sendpacket(handle, response_packet, sizeof(ethernet_hdr)+sizeof(ip)+sizeof(tcphdr) + strlen(content));
    free(response_packet);
  }

  pcap_close(handle);
  return 0;
}
